﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainUIControl : MonoBehaviour {

    public Text UItext;
    //public Button[] UIButtons;
    public CanvasGroup BtnGroup;

    float destAlpha;
    float fadeSpeed = 0.9f;

    bool userPressed = false;

	// Use this for initialization
	void Start () {
        BtnGroup.alpha = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.anyKey) userPressed = true;

        if (!userPressed)
        {
            TextAlphaControl(destAlpha, fadeSpeed);

            if (UItext.color.a <= 0.2f) destAlpha = 1;
            else if (UItext.color.a >= 0.92f) destAlpha = 0;
        }
        else
        {
            if (UItext.color.a <= 0.03)
            {
                Destroy(UItext);
                BtnGroup.alpha = Mathf.Lerp(BtnGroup.alpha, 1, Time.deltaTime * 1.2f);
            }
            else
            {
                TextAlphaControl(0, 2f);
            }
        }

	}

    void TextAlphaControl(float destAlpha, float fadespeed)
    {
        if (UItext == null) return;

        Color textcolor = UItext.color;
        textcolor.a = Mathf.Lerp(UItext.color.a, destAlpha, Time.deltaTime * fadespeed);

        UItext.color = textcolor;
    }
}
