﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingPageProcess : MonoBehaviour {

	void Start()
    {

    }

    void Awake ()
    {
        StartCoroutine(LoadNextScene());
	}


    IEnumerator LoadNextScene()
    {
        yield return new WaitForSeconds(2.0f);
        Debug.Log("LoadNextScene() Initiated");

        string nextSceneName = PlayerPrefs.GetString("NextScene");

        while (SceneInitiate.IsFinishedFading())
        {
            yield return new WaitForSeconds(0.2f);
        } 
        Debug.Log("Start to Load Next Scene");

        SceneInitiate.Fade(nextSceneName, Color.black, 1.2f, true);
    }
}
