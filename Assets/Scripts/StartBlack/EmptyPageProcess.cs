﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EmptyPageProcess : MonoBehaviour {

	// Use this for initialization
	void Awake () {
        int isExit = PlayerPrefs.GetInt("Exiting", 0);

        if (isExit == 1) StartCoroutine(ExitGame());
        else StartCoroutine(LoadMainPage());
	}
	

    IEnumerator LoadMainPage()
    {
        yield return new WaitForSeconds(0.2f);
        //SceneManager.LoadSceneAsync("main", LoadSceneMode.Single);
        SceneInitiate.Fade("main", Color.black, 1.2f, true);
    }

    IEnumerator ExitGame()
    {
        yield return new WaitForSeconds(0.2f);
        Application.Quit();
    }
}
