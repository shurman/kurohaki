﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuControl : MonoBehaviour {

    public Transform menuUI;
    public SD_Control charactor;

	// Use this for initialization
	void Start ()
    {
        menuUI.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(menuUI.gameObject.activeInHierarchy)
            {
                Resume();
                //Time.timeScale = 1;
            }
            else
            {
                menuUI.gameObject.SetActive(true);
                //Time.timeScale = 0;
                if (charactor != null) charactor.enable = false;
            }

        }
	}

    public void Resume()
    {
        menuUI.gameObject.SetActive(false);
        if (charactor != null) charactor.enable = true;
    }
}
