﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class ReflectProbeControl : MonoBehaviour
{
    public int updatePerFrame = 60;
    public bool follow;
    public GameObject followObject;
    public int heightOffset = 10;

    private int frameCounter;
    private int m_renderID;
    private ReflectionProbe reflectionProbe;
    private bool m_ObjIsMoving = false;
    private Vector3 m_lastLocation;

    // Start is called before the first frame update
    void Start()
    {
        reflectionProbe = gameObject.GetComponent<ReflectionProbe>();

        reflectionProbe.mode = ReflectionProbeMode.Realtime;
        reflectionProbe.refreshMode = ReflectionProbeRefreshMode.ViaScripting;
        reflectionProbe.timeSlicingMode = ReflectionProbeTimeSlicingMode.AllFacesAtOnce;

        reflectionProbe.center = new Vector3(0f, 0f, 0f);
        reflectionProbe.clearFlags = ReflectionProbeClearFlags.Skybox;
        reflectionProbe.size = new Vector3(0f, 300f, 0f);

        reflectionProbe.nearClipPlane = 40f;
        reflectionProbe.farClipPlane = 2000f;
    }

    void LateUpdate()
    {
        if (followObject != null)
        {
            if (m_lastLocation != followObject.transform.position)
            {
                m_lastLocation = followObject.transform.position;
                m_ObjIsMoving = true;
            }
            else
            {
                m_ObjIsMoving = false;
            }
        }

        if (follow) ProbeFollow();

        if (m_ObjIsMoving || (!m_ObjIsMoving && frameCounter != 0)) frameCounter++;

        if (frameCounter >= updatePerFrame)
        {
            if (reflectionProbe.IsFinishedRendering(m_renderID))
            {
                m_renderID = reflectionProbe.RenderProbe();
            }

            frameCounter = 0;
        }
    }

    void ProbeFollow()
    {
        if (followObject != null)
        {
            Vector3 location = followObject.transform.position;
            location.y += heightOffset;
            reflectionProbe.gameObject.transform.localPosition = location;
        }
    }
}
