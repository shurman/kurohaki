﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SD_Control : MonoBehaviour {

    private Animator anim;
    private AnimatorStateInfo currentBaseState;
    private Rigidbody rb;
    
    Quaternion charRotation;
    static Camera c_camera;
    static Transform cameraT;
    float forwardInput, turnInput, jumpInput;
    float vOrbitInput, hOrbitInput, zoomInput, hOrbitSnapInput;

    public bool enable = true;
    [Serializable]
    public class MoveSetting
    {
        public float forwardSpeed = 8.0f;
        public float backwardSpeed = 2.5f;
        public float rotateSpeed = 60.0f;
        public bool enableJump = false;
        public float jumpSpeed = 5;
        
    }

    [Serializable]
    public class PhySetting
    {
        public float dropAccel = 0.75f;
        public float groundDetectDist = 0.5f;
        public LayerMask ground;
        public float slopeSmoothForce = 40;
    }

    //[System.Serializable]
    //public class CameraSetting
    //{
    //    public Camera playerCamera;
    //    public float CameraSmooth = 0.09f;
    //    public Vector3 CameraOffset = new Vector3(0, 3f, -9.5f);
    //}

    [Serializable]
    public class CameraPosSetting
    {
        public Camera playerCamera;
        public float CameraLookSmooth = 50f;
        public Vector3 CameraOffset = new Vector3(0, 2f, 0f);

        public float distanceFromTarget = -7f;
        public float zoomSmooth = 160;
        public float maxZoom = -7f;
        public float minZoom = -14;

        public bool smoothFollow = true;
        public float CameraFollowSmooth = 2.5f;

        [HideInInspector]
        public float adjustedDistance = -9;
    }

    [Serializable]
    public class CameraOrbitSetting
    {
        public float xRotation = -8;
        public float yRotation = -180;
        public float maxXRotation = -4;
        public float minXRotation = -60;
        public float vOrbitSmooth = 60;
        public float hOrbitSmooth = 60;
    }

    [Serializable]
    public class CollisionHandler
    {
        public LayerMask collisionLayer;

        [HideInInspector]
        public bool colliding = false;
        [HideInInspector]
        public Vector3[] adjustedCameraClipPoints;
        [HideInInspector]
        public Vector3[] desiredCameraClipPoints;

        public CollisionHandler()
        {
            adjustedCameraClipPoints = new Vector3[5];
            desiredCameraClipPoints = new Vector3[5];
        }

        public void UpdateCameraClipPoints(Vector3 cameraPosition, Quaternion atRotation, ref Vector3[] intoArray)
        {
            intoArray = new Vector3[5];

            float z = c_camera.nearClipPlane;
            float x = Mathf.Tan(c_camera.fieldOfView / 3.41f) * z;
            float y = x / c_camera.aspect;

            //top left
            intoArray[0] = (atRotation * new Vector3(-x, y, z)) + cameraPosition;
            //top right
            intoArray[1] = (atRotation * new Vector3(x, y, z)) + cameraPosition;
            //bottom left
            intoArray[2] = (atRotation * new Vector3(-x, -y, z)) + cameraPosition;
            //bottom right
            intoArray[3] = (atRotation * new Vector3(x, -y, z)) + cameraPosition;
            //camera's position
            intoArray[4] = cameraPosition - cameraT.forward;

        }

        bool CollisionDetectedAtClipPoints(Vector3[] clipPoints, Vector3 fromPostion)
        {
            for (int i = 0; i < clipPoints.Length; i++)
            {
                Ray ray = new Ray(fromPostion, clipPoints[i] - fromPostion);
                float distance = Vector3.Distance(clipPoints[i], fromPostion);
                if (Physics.Raycast(ray, distance, collisionLayer))
                {
                    return true;
                }
            }

            return false;
        }

        public float GetAdjustedDistWithRayFrom(Vector3 from)
        {
            float distance = -1;

            for (int i = 0; i < desiredCameraClipPoints.Length; i++)
            {
                Ray ray = new Ray(from, desiredCameraClipPoints[i] - from);
                if (Physics.Raycast(ray, out RaycastHit hit))
                {
                    if (distance == -1) distance = hit.distance;
                    else
                    {
                        if (hit.distance < distance) distance = hit.distance;
                    }
                }
            }

            if (distance == -1) return 0;
            else return distance;
        }

        public void CheckColliding(Vector3 targetPosition)
        {
            if (CollisionDetectedAtClipPoints(desiredCameraClipPoints, targetPosition)) colliding = true;
            else colliding = false;
        }
    }

    [Serializable]
    public class DebugSettings
    {
        public bool drawDesiredCollisionLines = true;
        public bool drawAdjustedCollisionLines = true;
    }

    //float CameraRotateSpeed = 0;
    Vector3 Char_velocity = Vector3.zero;
    Vector3 Char_pos = Vector3.zero;
    bool jumplock = false;

    Vector3 destination = Vector3.zero;
    Vector3 adjustedDest = Vector3.zero;
    Vector3 camVel = Vector3.zero;

    public MoveSetting moveSetting = new MoveSetting();
    public PhySetting phySetting = new PhySetting();
    public CameraPosSetting cameraPosSetting = new CameraPosSetting();
    public CameraOrbitSetting cameraOrbSetting = new CameraOrbitSetting();
    public CollisionHandler collisionHandler = new CollisionHandler();
    public DebugSettings debugSetting = new DebugSettings();
    
    static readonly int SDStandingState = Animator.StringToHash("Base Layer.Standing@loop");
    static readonly int SDJumpToTopState = Animator.StringToHash("Base Layer.JumpToTop");
    static readonly int SDTopOfJumpState = Animator.StringToHash("Base Layer.TopOfJump");
    static readonly int SDTopToGroundState = Animator.StringToHash("Base Layer.TopToGround");
    static readonly int SDRunningState = Animator.StringToHash("Base Layer.Running@loop");

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();

        charRotation = transform.rotation;
        forwardInput = turnInput = 0;

        c_camera = cameraPosSetting.playerCamera;
        cameraT = cameraPosSetting.playerCamera.transform;

        CameraMoveToTarget();

        UpdateCameraClipPoints();

	}

    void Update()
    {
        GetInput();
        Turn();

        OrbitTarget();
        ZoomInOnTarget();
    }

    void FixedUpdate()
    {
        Run();
        //Jump();

        rb.velocity = transform.TransformDirection(Char_velocity);

        UpdateCameraClipPoints();

        for (int i = 0; i < 5; i++)
        {
            if(debugSetting.drawAdjustedCollisionLines)
            {
                Debug.DrawLine(Char_pos, collisionHandler.desiredCameraClipPoints[i], Color.white);
            }
            if(debugSetting.drawDesiredCollisionLines)
            {
                Debug.DrawLine(Char_pos, collisionHandler.adjustedCameraClipPoints[i], Color.green);
            }
        }

        collisionHandler.CheckColliding(Char_pos);
        cameraPosSetting.adjustedDistance = collisionHandler.GetAdjustedDistWithRayFrom(Char_pos);
	}

    void LateUpdate()
    {
        CameraMoveToTarget();
        CameraLookAtTarget();
    }

    void GetInput()
    {
        if (!enable)
        {
            forwardInput = 0;
            turnInput = 0;
            vOrbitInput = 0;
            hOrbitInput = 0;
            hOrbitSnapInput = 0;
            zoomInput = 0;
            return;
        }

        forwardInput = Input.GetAxis("Vertical");
        turnInput = Input.GetAxis("Horizontal");

        if (moveSetting.enableJump) jumpInput = Input.GetAxisRaw("Jump");
        else jumpInput = 0;

        vOrbitInput = Input.GetAxisRaw("OrbitVertical");
        hOrbitInput = Input.GetAxisRaw("OrbitHorizontal");
        hOrbitSnapInput = Input.GetAxisRaw("OrbitHorizontalSnap");
        zoomInput = Input.GetAxisRaw("Mouse ScrollWheel");

        //Debug.Log(vOrbitInput + " " + hOrbitInput + " " + hOrbitSnapInput);
    }

    void Run()
    {
        anim.SetFloat("MoveSpeed", forwardInput);

        float tmp_speed = (forwardInput > 0) ? moveSetting.forwardSpeed : moveSetting.backwardSpeed;
        Char_velocity.z = tmp_speed * forwardInput;

        if (Grounded(out RaycastHit raycast))
        {
            Char_velocity.y = -raycast.distance * phySetting.slopeSmoothForce;
            //Debug.Log($"{raycast.distance}__{Char_velocity.y}");
        }
    }

    void Turn()
    {
        charRotation *= Quaternion.AngleAxis(moveSetting.rotateSpeed * turnInput * Time.deltaTime, Vector3.up);
        transform.rotation = charRotation;
    }

    void Jump()
    {
        //Debug.Log(jumpInput + " " + Grounded());
        if (jumpInput > 0 && Grounded(out RaycastHit hit) && !jumplock)
        {
            jumplock = true;
            anim.SetBool("isJump", true);
            Char_velocity.y = moveSetting.jumpSpeed;
        }
        else if (jumpInput == 0 && Grounded(out hit))
        {
            Char_velocity.y = 0;
            anim.SetBool("isJump", false);

            jumplock = false;
        }
        else
        {
            Char_velocity.y -= phySetting.dropAccel;
        }

        //anim.SetBool("isFalling", !Grounded());
    }

    bool Grounded(out RaycastHit hit)
    {
#if UNITY_EDITOR
        // helper to visualise the ground check ray in the scene view
        Debug.DrawLine(transform.position, transform.position + (Vector3.down * phySetting.groundDetectDist));
#endif
        return Physics.Raycast(transform.position, Vector3.down, out hit, phySetting.groundDetectDist, phySetting.ground);
    }

    string AniHashNameConvert(int hash)
    {
        if (hash == SDRunningState) return "Running";
        else if (hash == SDJumpToTopState) return "JumpToTop";
        else if (hash == SDStandingState) return "Standing";
        else if (hash == SDTopOfJumpState) return "TopOfJump";
        else if (hash == SDTopToGroundState) return "TopToGround";
        else return "N/A";
    }

    void UpdateCameraClipPoints()
    {
        collisionHandler.UpdateCameraClipPoints(cameraT.position, cameraT.rotation, ref collisionHandler.adjustedCameraClipPoints);
        collisionHandler.UpdateCameraClipPoints(destination, cameraT.rotation, ref collisionHandler.desiredCameraClipPoints);
    }

    void CameraMoveToTarget()
    {
        //camera.position = transform.position + charRotation * camerasetting.CameraOffset;

        Char_pos = transform.position + cameraPosSetting.CameraOffset;

        destination = Quaternion.Euler(cameraOrbSetting.xRotation, cameraOrbSetting.yRotation + transform.eulerAngles.y, 0) * -Vector3.forward * cameraPosSetting.distanceFromTarget;
        destination += Char_pos;
        //cameraT.position = destination;
        //Debug.Log(destination- cameraT.position);

        if (collisionHandler.colliding)
        {
            adjustedDest = Quaternion.Euler(cameraOrbSetting.xRotation, cameraOrbSetting.yRotation + transform.eulerAngles.y, 0) * Vector3.forward * cameraPosSetting.adjustedDistance;
            adjustedDest += Char_pos;

            if (cameraPosSetting.smoothFollow)
                //cameraT.position = Vector3.SmoothDamp(cameraT.position, adjustedDest, ref camVel, cameraPosSetting.CameraFollowSmooth);
                cameraT.position = Vector3.Lerp(cameraT.position, adjustedDest, cameraPosSetting.CameraFollowSmooth * Time.deltaTime);
            else cameraT.position = adjustedDest;
        }
        else
        {
            if (cameraPosSetting.smoothFollow)
                //cameraT.position = Vector3.SmoothDamp(cameraT.position, destination, ref camVel, cameraPosSetting.CameraFollowSmooth);
                cameraT.position = Vector3.Lerp(cameraT.position, destination, cameraPosSetting.CameraFollowSmooth * Time.deltaTime);
            else cameraT.position = destination;
        }
    }

    void CameraLookAtTarget()
    {
        //float eulerYAngle = Mathf.SmoothDampAngle(camera.eulerAngles.y, transform.eulerAngles.y, ref CameraRotateSpeed, camerasetting.CameraSmooth);
        //camera.rotation = Quaternion.Euler(camerasetting.playerCamera.transform.eulerAngles.x, eulerYAngle, 0);

        Quaternion targRot = Quaternion.LookRotation(Char_pos - cameraT.position);
        cameraT.rotation = Quaternion.Lerp(cameraT.rotation, targRot, cameraPosSetting.CameraLookSmooth * Time.deltaTime);
    }

    void OrbitTarget()
    {
        if (hOrbitSnapInput > 0)
        {
            cameraOrbSetting.xRotation = -10;
            cameraOrbSetting.yRotation = -180;
        }

        cameraOrbSetting.xRotation += -vOrbitInput * cameraOrbSetting.vOrbitSmooth * Time.deltaTime;
        cameraOrbSetting.yRotation += -hOrbitInput * cameraOrbSetting.hOrbitSmooth * Time.deltaTime;

        if (cameraOrbSetting.xRotation > cameraOrbSetting.maxXRotation)
        {
            cameraOrbSetting.xRotation = cameraOrbSetting.maxXRotation;
        }
        if (cameraOrbSetting.xRotation < cameraOrbSetting.minXRotation)
        {
            cameraOrbSetting.xRotation = cameraOrbSetting.minXRotation;
        }
    }

    void ZoomInOnTarget()
    {
        cameraPosSetting.distanceFromTarget += zoomInput * cameraPosSetting.zoomSmooth * Time.deltaTime;

        if (cameraPosSetting.distanceFromTarget > cameraPosSetting.maxZoom)
        {
            cameraPosSetting.distanceFromTarget = cameraPosSetting.maxZoom;
        }
        if (cameraPosSetting.distanceFromTarget < cameraPosSetting.minZoom)
        {
            cameraPosSetting.distanceFromTarget = cameraPosSetting.minZoom;
        }
    }



}
