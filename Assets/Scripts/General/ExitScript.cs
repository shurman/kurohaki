﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitScript : MonoBehaviour {

	public void Exit()
    {
        PlayerPrefs.SetInt("Exiting", 1);

        SceneInitiate.Fade("EmptyBlack", Color.black, 1.2f);
        //Application.Quit();
    }
}
