﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadSceneAsyc : MonoBehaviour {

    public string loadSceneName;

	// Use this for initialization
	public void Click()
    {
        PlayerPrefs.SetString("NextScene", loadSceneName);

        SceneInitiate.Fade("LoadingScene", Color.black, 1.2f);
	}

}
