﻿using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    public int[] DialogueId;

    private GameObject sphere;

    //private DialogueControl dlgControl;

    // Start is called before the first frame update
    void Start()
    {
        //dlgControl = (DialogueControl)Camera.main.GetComponent(typeof(DialogueControl));
        //dlgControl = (DialogueControl)GameObject.Find("Utc_win_humanoid").GetComponent(typeof(DialogueControl));
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CreateTalkIcon()
    {
        sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);

        sphere.name = "talk_icon";

        sphere.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
        sphere.transform.position = transform.position + new Vector3(0, 2f, 0);
    }
    public void DestroyTalkIcon()
    {
        if(sphere) Destroy(sphere);
    }

    public void HideTalkIcon()
    {
        sphere.SetActive(false);
    }
    public void ShowTalkIcon()
    {
        sphere.SetActive(true);
    }
}
