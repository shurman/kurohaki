﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;

public class DialogueControl : MonoBehaviour
{
    public GameObject dialogueBoxUI;
    public Text textName;
    public Text textDialogue;

    public GameObject BlackEdgeUI;

    public GameObject UILookAt;

    [HideInInspector]
    public bool isTyping = false;

    private Dictionary<int, DlgItem> DialogueContent = new Dictionary<int, DlgItem>();
    private SD_Control sdcontrol_obj;
    private Queue<DlgItem> DlgQueue = new Queue<DlgItem>();

    private Animation AniBlackEdge;

    private List<GameObject> NearCharList = new List<GameObject>();
    private GameObject CurNearChar;
    private GameObject LastNearChar = null;

    void Start()
    {
        sdcontrol_obj = (SD_Control)GetComponent(typeof(SD_Control));
        AniBlackEdge = (Animation)BlackEdgeUI.GetComponent(typeof(Animation));

        LoadDialogue();
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag != "NPC") return;

        NearCharList.Add(collider.gameObject);

        Debug.Log($"DialogueControl OnTriggerEnter {collider.gameObject.name}");
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.tag != "NPC") return;

        NearCharList.Remove(collider.gameObject);

        Debug.Log($"DialogueControl OnTriggerExit {collider.gameObject.name}");
    }

    //public void DisplaySentence(int id)
    //{
    //    //StopAllCoroutines();
    //    StartCoroutine(TypeSentence(DialogueContent[id].name, DialogueContent[id].conversation));
    //}

    public void DisplaySentences(int[] idset)
    {
        AniBlackEdge["AniBlackEdge"].speed = 1;
        AniBlackEdge.Play();

        foreach (int id in idset)
        {
            DlgQueue.Enqueue(DialogueContent[id]);
        }

        StopAllCoroutines();
        StartCoroutine(TypeSentence(DlgQueue.Dequeue()));
    }

    private IEnumerator TypeSentence(DlgItem dlg_itm)
    {
        isTyping = true;

        string name = dlg_itm.name;
        string conversation = dlg_itm.conversation;

        //dialogueBoxUI.GetComponent<RectTransform>().anchoredPosition = new Vector2(dlg_itm.pos_x, dlg_itm.pos_y);

        Debug.Log($"{dialogueBoxUI.transform.position} {conversation}");
        sdcontrol_obj.enable = false;
        dialogueBoxUI.gameObject.SetActive(true);

        textName.text = name;
        textDialogue.text = "";
        foreach (char letter in conversation.ToCharArray())
        {
            textDialogue.text += letter;
            yield return new WaitForSeconds(0.01f);
        }
    }

    void Update()
    {
        if (NearCharList.Count != 0)
        {
            float minDis = 999;

            foreach (GameObject obj in NearCharList)
            {
                float objDis = Vector3.Distance(gameObject.transform.position, obj.transform.position);

                if (objDis < minDis)
                {
                    minDis = objDis;
                    CurNearChar = obj;
                }
            }

            if (CurNearChar.name != LastNearChar?.name)
            {
                ((DialogueTrigger)LastNearChar?.GetComponent(typeof(DialogueTrigger)))?.DestroyTalkIcon();
                ((DialogueTrigger)CurNearChar.GetComponent(typeof(DialogueTrigger))).CreateTalkIcon();

                LastNearChar = CurNearChar;
            }
        }
        else
        {
            ((DialogueTrigger)LastNearChar?.GetComponent(typeof(DialogueTrigger)))?.DestroyTalkIcon();
            LastNearChar = null;
        }


        if (Input.GetKeyUp(KeyCode.E))
        {
            DialogueTrigger nearObjDlg = (DialogueTrigger)CurNearChar.GetComponent(typeof(DialogueTrigger));

            if (dialogueBoxUI.activeSelf)
            {
                if (DlgQueue.Count == 0)
                {
                    AniBlackEdge["AniBlackEdge"].speed = -1;
                    AniBlackEdge["AniBlackEdge"].normalizedTime = 1;
                    AniBlackEdge.Play();

                    Debug.Log("Closing");
                    isTyping = false;
                    dialogueBoxUI.gameObject.SetActive(false);
                    nearObjDlg.ShowTalkIcon();

                    sdcontrol_obj.enable = true;
                }
                else
                {
                    StopAllCoroutines();
                    StartCoroutine(TypeSentence(DlgQueue.Dequeue()));
                }
            }
            else if (!isTyping)
            {
                if (NearCharList.Count != 0)
                {
                    Debug.Log("Clicked");
                    int[] ArrDialogueId = nearObjDlg.DialogueId;

                    Debug.Log("Talk to " + CurNearChar.name);
                    dialogueBoxUI.transform.position = CurNearChar.transform.position + new Vector3(0, 2.5f, 0);
                    dialogueBoxUI.transform.rotation = Quaternion.LookRotation(dialogueBoxUI.transform.position - UILookAt.transform.position);

                    nearObjDlg.HideTalkIcon();

                    DisplaySentences(ArrDialogueId);
                }
            }
        }
    }

    private void LoadDialogue()
    {
        XmlDocument document = new XmlDocument();

        document.Load(Application.dataPath + "/Scripts/General/Dialogue/Dialog.xml");

        XmlElement rootEle = document.LastChild as XmlElement;

        foreach (XmlElement ele in rootEle.ChildNodes)
        {
            DlgItem itm = new DlgItem();
            int id = -1;
            foreach (XmlElement node in ele.ChildNodes)
            {
                if (node.Name == "id") int.TryParse(ele.ChildNodes[0].InnerText, out id);
                else if (node.Name == "type") Enum.TryParse(node.InnerText, out itm.type);
                else if (node.Name == "name") itm.name = node.InnerText;
                else if (node.Name == "content") itm.conversation = node.InnerText;
            }

            DialogueContent.Add(id, itm);

            Debug.Log($"{id} {itm.name} {itm.conversation}");
        }
    }
}

public enum DlgType { speak, think }
public class DlgItem
{
    public DlgType type;
    public string name;
    public string conversation;
}
