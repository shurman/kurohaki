﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ReplaceSplatmap : EditorWindow
{
    public Texture2D Splatmap;
    public Texture2D New;
    public bool FlipVertical;

    [MenuItem("Terrain/Replace Splatmap")]
    static void Replace()
    {
        EditorWindow window = GetWindow<ReplaceSplatmap>();
        //window.position = new Rect(50f, 50f, 200f, 24f);
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.Label("Replace the existing splatmap of your terrain with a new one.\nDrag the embedded splatmap texture of your terrain to the 'Splatmap box'.\nThen drag the replacement splatmap texture to the 'New' box\nThen hit 'Replace'.");

        Splatmap = (Texture2D)EditorGUILayout.ObjectField("Splatmap", Splatmap, typeof(Texture2D), true);
        New = (Texture2D)EditorGUILayout.ObjectField("New", New, typeof(Texture2D), true);
        FlipVertical = GUILayout.Toggle(FlipVertical, "Flip Vertical");


        if(GUILayout.Button("Replace"))
        {
            if (New.format != TextureFormat.RGBA32 && New.format != TextureFormat.ARGB32 && New.format != TextureFormat.RGB24)
            {
                EditorUtility.DisplayDialog("Wrong format", "Splatmap must be set to the RGBA 32 bit format in the Texture Inspector.\nMake sure the type is Advanced and set the format!", "Cancel");
                return;
            }

            int w = New.width;
            if (Mathf.ClosestPowerOfTwo(w) != w)
            {
                EditorUtility.DisplayDialog("Wrong size", "Splatmap width and height must be a power of two!", "Cancel");
                return;
            }

            try
            {
                Color[] pixels = New.GetPixels();

                if (FlipVertical)
                {
                    int h = w; // always square in unity
                    for (int y = 0; y < h / 2; y++)
                    {
                        var otherY = h - y - 1;
                        for (var x = 0; x < w; x++)
                        {
                            Color swapval = pixels[y * w + x];
                            pixels[y * w + x] = pixels[otherY * w + x];
                            pixels[otherY * w + x] = swapval;
                        }
                    }
                }

                Splatmap.Resize(New.width, New.height, New.format, true);
                Splatmap.SetPixels(pixels);
                Splatmap.Apply();
            }
            catch
            {
                EditorUtility.DisplayDialog("Not readable", "The 'New' splatmap must be readable. Make sure the type is Advanced and enable read/write and try again!", "Cancel");
                return;
            }
        }
    }
}
